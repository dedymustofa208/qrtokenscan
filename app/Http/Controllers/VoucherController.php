<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use DB;
use Auth;
use Carbon\Carbon;
use App\Models\Voucher;
use Illuminate\Support\Str;
use App\Models\User;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\Storage;
use File;

class VoucherController extends Controller
{
    public function getData(Request $request){
        if($request->ajax()){
            $data = Voucher::orderBy('created_at','DESC')->get();
            return DataTables::of($data)
            ->addColumn('action', function($data){
                if($data->path_qr != null && $data->activated == false){
                    return '<button class="btn btn-danger" data-id="'.$data->id.'" onclick="return deleteThis(event);"><i class="icon icon-trash"></i> DELETE</button> 
                    <button class="btn btn-secondary" data-id="'.$data->id.'" onclick="return activatedKey(event);"><i class="icon icon-unlock"></i> ACTIVATED</button> 
                    <button class="btn btn-info" data-id="'.$data->id.'" onclick="return deleteQr(event);"><i class="icon icon-barcode"></i> DELETE QR</button> 
                    <a href="'.route('voucher.qrScan', $data->id).'" target="_blank" class="btn btn-warning"><i class="icon icon-eye-open"></i> PREVIEW</a>
                    ';
                }elseif($data->path_qr != null && $data->activated == true){
                    return '<button href="#" class="btn btn-outline-warning" disabled><i class="icon icon-eye-open"></i> PREVIEW</button>
                    ';
                }
                else{
                    return '<button class="btn btn-danger" data-id="'.$data->id.'" onclick="return deleteThis(event);"><i class="icon icon-trash"></i> DELETE</button> 
                    <button class="btn btn-success" data-id="'.$data->id.'" onclick="return genQR(event);"><i class="icon icon-barcode"></i> GENERATE QR</button> ';
                }
            })
            ->addColumn('qr_code',function($data){
                if($data->path_qr == null){
                    return '<img src="/images/thumbnail.svg" alt="QR Code" width="100" height="100">';
                }else{
                    $image_path = asset($data->path_qr); // Get the URL of the QR code image
                    return '<img src="' . $image_path . '" alt="QR Code" width="100" height="100">';
                }
            })
            ->rawColumns(['action','qr_code'])
            ->make(true);
        }
    }
    public function storedCode(Request $request){
        $voucher_code   = $request->gen_voucher;
        $token          = Str::uuid()->toString();
        $otp_code       = $this->generateUniqueOTP();
        try {
            DB::begintransaction();
                Voucher::firstOrCreate([
                    'voucher_code'      => $voucher_code,
                    'token'             => $token,
                    'otp_code'          => $otp_code
                ]);
            DB::commit();
            return response()->json(200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json(422);
        }
    }

    private function generateUniqueOTP()
    {
        $otp_code = '';
        $otp_exists = true;
        while ($otp_exists) {
            for ($i = 0; $i < 5; $i++) {
                $otp_code .= rand(0, 9);
            }
            $check_otp = Voucher::where('otp_code', $otp_code)->whereNull('deleted_at')->first();

            if ($check_otp === null) {
                $otp_exists = false;
            } else {
                $otp_code = '';
            }
        }
        return $otp_code;
    }


    public function generateVoucherCode(Request $request){
        $user = $request->user;
        $check_user = User::where('id',$user)->exists();
        if($check_user){
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            $length = 10;
            $code = '';
            $voucher_code = '';
            $code_exists = true;
            while ($code_exists) {
                // Generate a new OTP code
                for ($i = 0; $i < $length; $i++) {
                    $voucher_code .= $characters[rand(0, strlen($characters) - 1)];
                }
                $check_otp = Voucher::where('voucher_code', $voucher_code)->whereNull('deleted_at')->first();

                if ($check_otp === null) {
                    $code_exists = false;
                } else {
                    $voucher_code = '';
                }
            }
            return response()->json($voucher_code,200);
        }else{
            return response()->json(422);
        }
    }

    public function deleteVoucher(Request $request){
        $id = $request->id;
        try {
            DB::begintransaction();
                Voucher::where('id',$id)->delete();
            DB::commit();
            return response()->json(200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json(422);
        }
    }

    public function activatedCode(Request $request){
        $id = $request->id;
        try {
            DB::begintransaction();
                Voucher::where('id',$id)->update([
                    'activated' => true,
                    'otp_code'  => null
                ]);
            DB::commit();
            return response()->json(200);
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json(422);
        }
    }

    public function generateQR(Request $request)
    {
        $id             = $request->id;
        $qr_exists      = Voucher::where('id', $id)->whereNotNull('path_qr')->whereNull('deleted_at')->first();
        $url            = request()->getSchemeAndHttpHost();
        $gen_qr_code    = $url.'/qr-scan/'.$id;
        $filename       = 'images/qr-code/qr_'.Carbon::now()->format('u').'.svg';
        $path           = public_path($filename);
        if($qr_exists != null){
            return response()->json('QR Already Exists!',422);
        }
        try {
            DB::beginTransaction();
            QrCode::generate($gen_qr_code, $path);
            Voucher::where('id', $id)->update([
                'path_qr' => $filename
            ]);
            DB::commit();
            return response()->json(200);
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json(422);
        }
        
    }

    public function deleteQrCode(Request $request){
        $id = $request->id;
        $get_name = Voucher::where('id',$id)->whereNull('deleted_at')->whereNotNull('path_qr')->first();
        if($get_name != null){
            $file = $get_name->path_qr;
            try {
                DB::beginTransaction();
                if(File::exists(public_path($file))){
                    File::delete(public_path($file));
                    Voucher::where('id', $id)->update([
                        'path_qr' => null
                    ]);
                    DB::commit();
                    return response()->json(200);
                }else{
                    return response()->json('QR Not Deleted!',422);
                }
            } catch (Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                ErrorHandler::db($message);
                return response()->json(422);
            }
        }else{
            return response()->json('QR Code Does not Exists',422);
        }
    }

    public function redeemCode(Request $request){
        $otp_code = $request->otp_;
        $validate = Voucher::where('otp_code',$otp_code)->first();
        if($validate != null){
            try {
                DB::begintransaction();
                    Voucher::where('otp_code',$otp_code)->update([
                        'activated'     => true,
                        'otp_code'      => null,
                        'deleted_at'    => Carbon::now()
                    ]);
                DB::commit();
                return response()->json(200);
            }catch (Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                ErrorHandler::db($message);
                return response()->json(422);
            }
        }else{
            return response()->json('Your OTP Invalid!',422);
        }
    }

    public function qrScan($id){
        $voucher_data = Voucher::where('id',$id)->whereNull('deleted_at')->first();
        if($voucher_data != null){
            return view('qr.index',compact('voucher_data'));
        }else{
            return view('qr.index2');
        }
    }
}
