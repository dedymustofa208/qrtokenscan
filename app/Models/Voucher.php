<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Voucher extends Model
{
    use Uuids;
    protected $table = 'vouchers';
	protected $fillable = ['voucher_code','activated','token','deleted_at','otp_code','path_qr'];
    protected $guarded = ['id'];
    protected $dates = ['created_at','updated_at'];
}
