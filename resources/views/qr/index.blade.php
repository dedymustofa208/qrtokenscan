@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-4 col-md-4">
            <div class="dl">
                    <form class="form-horizontal" action="#" method="POST"  id="redeem_code_form" enctype="multipart/form-data">
                        @csrf
                        <div class="brand">
                            <h2>{{ $voucher_data->voucher_code }}</h2>
                        </div>
                        <div class="discount alizarin">50%
                            <div class="type">OFF</div>
                            <div class="type">REDEEM THIS CODE</div>
                        </div>
                        <div class="coupon midnight-blue">
                            <label class="badge bg-primary text-white mb-3" >{{ $voucher_data->otp_code }}</label>
                            <input type="number" class="form-control mb-2" id="opt_" name="otp_"></input>
                            <button type="submit" class="btn btn-success" id="btn-redeem" name="btn-redeem">REDEEM</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('page-js')
<script>
$(document).ready(function(){
    document.getElementById("btn-redeem").addEventListener("click", function(event) {
        event.preventDefault();
        var otpCode = $('#otp_').val();
        if(otpCode === ''){
            alert('Voucher Cannot Empty');
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : "{{ route('voucher.redeemCode') }}",
            data: $('#redeem_code_form').serialize(),
            beforeSend: function () {
                $('#redeem_code_form').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('#redeem_code_form').unblock();
            },
            success: function(response) {
                Swal.fire({
                    icon:  'success',
                    title: 'Good',
                    text: 'Your OTP Verified!',
                });
                setTimeout(() => {
                    window.location.href = 'http://192.168.1.9:8000/home';
                }, 3000);
            },
            error: function(response) {
                if (response.status == 422){
                    Swal.fire({
                        icon:  'error',
                        title: 'Oopss...',
                        text: response.responseJSON,
                    });
                }
            }
        })
    });
})
</script>
@endsection