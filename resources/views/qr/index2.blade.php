@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-4 col-md-4">
            <div class="dl">
                <div class="brand">
                    <h2>VOUCER EXPIRED</h2>
                </div>
                <div class="discount alizarin">50%
                    <div class="type">OFF</div>
                    <div class="type">CODE UNAVAILABLE</div>
                </div>
                <div class="coupon midnight-blue">
                    <label class="badge bg-primary text-white mb-3" >000000</label>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection