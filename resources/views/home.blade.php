@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <form class="form-horizontal" action="#" method="POST"  id="voucher_code_form" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="gen_voucher" id="gen_voucher" placeholder="Generate Your Voucher Code" aria-label="Voucher Code" aria-describedby="button-addon2">
                            <button type="button" class="btn btn-outline-secondary" id="btn-generate" name="btn-generate">Generate</button>
                            <button type="submit" class="btn btn-outline-success" id="btn-submit" name="btn-submit">SUBMIT</button>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="table-list">
                                <thead class="bg-teal">
                                    <tr>
                                        <th>No</th>
                                        <th>Voucer Code</th>
                                        <th>Is Active</th>
                                        <th>OTP</th>
                                        <th>QR CODE</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script>
$(document).ready(function(){
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });
    var table = $('#table-list').DataTable({
        ajax: "{{ route('voucher.getData') }}",
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'voucher_code', name: 'voucher_code'},
            {data: 'activated', name: 'activated'},
            {data: 'otp_code', name: 'otp_code'},
            {data: 'qr_code', name: 'qr_code'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
    document.getElementById("btn-submit").addEventListener("click", function(event) {
        event.preventDefault();
        var genCode = $('#gen_voucher').val();
        if(genCode === ''){
            alert('Voucher Cannot Empty');
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : "{{ route('voucher.storedCode') }}",
            data: $('#voucher_code_form').serialize(),
            beforeSend: function () {
                $('#voucher_code_form').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('#voucher_code_form').unblock();
            },
            success: function(response) {
                $('#gen_voucher').val(null);
                Swal.fire({
                    icon:  'success',
                    title: 'Horee...',
                    text: 'Voucher Created!',
                });
                table.draw();
            },
            error: function(response) {
                if (response.status == 422){
                    Swal.fire({
                        icon:  'error',
                        title: 'Oopss...',
                        text: 'Something Went Wrong!',
                    });
                }

            }
        })
    });
    // Add click event listener to "Generate" button
    document.getElementById("btn-generate").addEventListener("click", function() {
        // var voucherCode = generateCode(); // Generate voucher code
        // document.getElementById("gen_voucher").value = voucherCode; // Update input field with voucher code
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : "{{ route('voucher.generateVoucherCode') }}",
            data: {
                user:"{{Auth::user()->id}}"
            },
            beforeSend: function () {
                $('#voucher_code_form').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('#voucher_code_form').unblock();
            },
            success: function(response) {
                $('#gen_voucher').val(response);
            },
            error: function(response) {
                if (response.status == 422){
                    alert('Something Went Wrong!');
                }
            }
        })
    });
});
function deleteThis(event) {
    // Prevent the default action of the button
    event.preventDefault();
    var id = event.target.getAttribute('data-id');
    var row = event.target.closest('tr');    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    bootbox.confirm("Are you sure delete this row ?", function (result) {
        if (result) {
            $.ajax({
                url: "{{ route('voucher.deleteVoucher') }}",
                data:{
                    id:id
                },
                method: 'POST',
                success: function(response) {
                    Swal.fire({
                        icon:  'info',
                        title: 'Horee...',
                        text: 'Voucher Deleted!',
                    });
                    $('#table-list').DataTable().ajax.reload();
                },
                error: function(xhr) {
                    Swal.fire({
                        icon:  'error',
                        title: 'Oopss...',
                        text: 'Something Went Wrong!',
                    });
                    $('#table-list').DataTable().ajax.reload();
                }
            });
        }
    });
}

function activatedKey(event) {
    // Prevent the default action of the button
    event.preventDefault();
    var id = event.target.getAttribute('data-id');
    var row = event.target.closest('tr');    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    bootbox.confirm("Are you sure activated this voucher ?", function (result) {
        if (result) {
            $.ajax({
                url: "{{ route('voucher.activatedCode') }}",
                data:{
                    id:id
                },
                method: 'POST',
                success: function(response) {
                    Swal.fire({
                        icon:  'success',
                        title: 'Horee...',
                        text: 'Voucher Activated!',
                    });
                    $('#table-list').DataTable().ajax.reload();
                },
                error: function(xhr) {
                    Swal.fire({
                        icon:  'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    });
                    $('#table-list').DataTable().ajax.reload();
                }
            });
        }
    });
}
function genQR(event) {
    // Prevent the default action of the button
    event.preventDefault();
    var id = event.target.getAttribute('data-id');
    var row = event.target.closest('tr');    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    bootbox.confirm("Are you sure generated QR?", function (result) {
        if (result) {
            $.ajax({
                url: "{{ route('voucher.generateQR') }}",
                data:{
                    id:id
                },
                method: 'POST',
                success: function(response) {
                    Swal.fire({
                        icon:  'success',
                        title: 'Horee...',
                        text: 'QR generated!',
                    });
                    $('#table-list').DataTable().ajax.reload();
                },
                error: function(xhr) {
                    Swal.fire({
                        icon:  'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    });
                    $('#table-list').DataTable().ajax.reload();
                }
            });
        }
    });
}
function deleteQr(event) {
    // Prevent the default action of the button
    event.preventDefault();
    var id = event.target.getAttribute('data-id');
    var row = event.target.closest('tr');    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    bootbox.confirm("Are you sure Delete this QR?", function (result) {
        if (result) {
            $.ajax({
                url: "{{ route('voucher.deleteQrCode') }}",
                data:{
                    id:id
                },
                method: 'POST',
                success: function(response) {
                    Swal.fire({
                        icon:  'success',
                        title: 'Horee...',
                        text: 'QR Deleted!',
                    });
                    $('#table-list').DataTable().ajax.reload();
                },
                error: function(response) {
                    Swal.fire({
                        icon:  'error',
                        title: 'Oops...',
                        text: response.responseJSON,
                    });
                    $('#table-list').DataTable().ajax.reload();
                }
            });
        }
    });
}
function preview(event) {
    // Prevent the default action of the button
    event.preventDefault();
    var id = event.target.getAttribute('data-id');
    var row = event.target.closest('tr');    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    bootbox.confirm("Are you sure preview this Voucher?", function (result) {
        if (result) {
            $.ajax({
                url: "/qr-scan/"+id,
                method: 'POST',
                success: function(response) {
                    
                },
                error: function(response) {
                    Swal.fire({
                        icon:  'error',
                        title: 'Oops...',
                        text: response.responseJSON,
                    });
                }
            });
        }
    });
}

</script>
@endsection
