<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/get-data', [App\Http\Controllers\VoucherController::class, 'getData'])->name('voucher.getData');
Route::post('/gen-voucher', [App\Http\Controllers\VoucherController::class, 'generateVoucherCode'])->name('voucher.generateVoucherCode');
Route::post('/store-code', [App\Http\Controllers\VoucherController::class, 'storedCode'])->name('voucher.storedCode');
Route::post('/delete-voucher', [App\Http\Controllers\VoucherController::class, 'deleteVoucher'])->name('voucher.deleteVoucher');
Route::post('/activated-voucher', [App\Http\Controllers\VoucherController::class, 'activatedCode'])->name('voucher.activatedCode');
Route::post('/generate-qr-code', [App\Http\Controllers\VoucherController::class, 'generateQR'])->name('voucher.generateQR');
Route::post('/redeem-code', [App\Http\Controllers\VoucherController::class, 'redeemCode'])->name('voucher.redeemCode');
Route::post('/delete-qr-code', [App\Http\Controllers\VoucherController::class, 'deleteQrCode'])->name('voucher.deleteQrCode');
Route::get('/qr-scan/{id}', [App\Http\Controllers\VoucherController::class, 'qrScan'])->name('voucher.qrScan');

